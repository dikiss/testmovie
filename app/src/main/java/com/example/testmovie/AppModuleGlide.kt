package com.example.testmovie

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

/**
 * Created with Diana Kisil
 */

@GlideModule
class AppModuleGlide : AppGlideModule()