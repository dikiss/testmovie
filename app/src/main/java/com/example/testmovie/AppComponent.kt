package com.example.testmovie


import dagger.Component
import javax.inject.Singleton

/**
 * @author Diana Kisil
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class, ApiModule::class))
interface AppComponent {
    fun plus(listingModule: ListingModule): ListingComponent
}