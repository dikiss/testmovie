package com.example.testmovie.ui

import javax.inject.Scope

/**
 * @author Diana Kisil
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ListingScope