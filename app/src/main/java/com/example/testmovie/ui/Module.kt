package com.example.testmovie.ui

import com.example.testmovie.model.MovieApi
import dagger.Module
import dagger.Provides

/**
 * @author Diana Kisil
 */
@Module
class ListingModule {

    @Provides
    fun provideListingPresenter(listingInteractor: ListingInteractor): Presenter {
        return ListingPresenterImpl(listingInteractor, null)
    }

    @Provides @ListingScope
    fun provideListingInteractor(api: MovieApi): ListingInteractor {
        return ListingInteractorImpl(api)
    }
}