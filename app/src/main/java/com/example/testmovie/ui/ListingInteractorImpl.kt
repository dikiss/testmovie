package com.example.testmovie.ui

import com.example.testmovie.model.MovieApi
import com.example.testmovie.model.PopularMoviesResponse
import rx.Observable

/**
 * @author Diana Kisil
 */
class ListingInteractorImpl(val movieDbApi: MovieApi) : ListingInteractor {

    override fun getListOfMovies(): Observable<PopularMoviesResponse> {
        return movieDbApi.getVenues(createQueryMap())
    }

    private fun createQueryMap(): Map<String, String> {
        return hashMapOf(
            "language" to "en",
            "sort_by" to "popularity.desc")
    }
}