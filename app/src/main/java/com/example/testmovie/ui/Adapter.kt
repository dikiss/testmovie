package com.example.testmovie.ui

import android.graphics.Movie
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * @author Diana Kisil
 */

class Adapter : RecyclerView.Adapter<Adapter.ViewHolder>() {

    private var movies: List<Movie> = ArrayList()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(movies[position])
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val root = (LayoutInflater.from(parent.context).inflate(R.layout.movie_grid_item, parent, false))
        return ViewHolder(root)
    }

    fun addMovies(movies: List<Movie>?) {
        if (movies != null) {
            this.movies = movies
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(root: View) : RecyclerView.ViewHolder(root) {
        fun bind(movie: Movie) = with(itemView) {
            title.text = movie.title

            GlideApp.with(context).load(movie.getPosterUrl()).into(poster)
        }
    }
}