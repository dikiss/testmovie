package com.example.testmovie.ui

import com.example.testmovie.Movie

/**
 * @author Diana Kisil
 */
interface ListView {
    fun showMovies(movies: List<Movie>?)
}