package com.example.testmovie.ui

import com.example.testmovie.model.PopularMoviesResponse
import rx.Observable

/**
 * @author Diana Kisil
 */
interface ListingInteractor {
    fun getListOfMovies() : Observable<PopularMoviesResponse>
}