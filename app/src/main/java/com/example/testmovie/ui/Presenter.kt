package com.example.testmovie.ui

/**
 * @author Diana Kisil
 */
interface Presenter {
    fun setView(listingView: ListView)
}