package com.example.testmovie.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.testmovie.AppMovie
import com.example.testmovie.Movie
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

/**
 * @author Diana Kisil
 */
class FragmentList : Fragment(), ListView {
    @Inject
    lateinit var presenter: Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context?.applicationContext as AppMovie).createListingComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initLayout()
        presenter.setView(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (context?.applicationContext as AppMovie).releaseListingComponent()
    }

    private fun initLayout() {
        movies_listing.setHasFixedSize(true)
        val layoutManager = GridLayoutManager(context, 2)
        movies_listing.layoutManager = layoutManager
        movies_listing.setHasFixedSize(true)
        movies_listing.adapter = Adapter()
    }

    override fun showMovies(movies: List<Movie>?) {
        (movies_listing.adapter as Adapter).addMovies(movies)
    }
}