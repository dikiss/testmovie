package com.example.testmovie.ui

import android.util.Log
import com.example.testmovie.model.PopularMoviesResponse
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

/**
 * @author Diana Kisil
 */
class ListingPresenterImpl(val interactor: ListingInteractor, private var view: ListView?) : Presenter {

    override fun setView(listingView: ListView) {
        view = listingView
        getListOfMovies()
    }

    private fun getListOfMovies() {
        interactor.getListOfMovies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { popularMoviesResponse -> onGetMoviewsSuccess(popularMoviesResponse)},
                { e -> onGetMoviesFailure(e) }
            )
    }

    private fun onGetMoviesFailure(e: Throwable?) {
        Log.e(e?.message, e?.stackTrace.toString())
    }

    private fun onGetMoviewsSuccess(popularMoviesResponse: PopularMoviesResponse?) {
        view?.showMovies(popularMoviesResponse?.movies)
    }
}