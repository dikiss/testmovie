package com.example.testmovie.ui

import dagger.Subcomponent

/**
 * @author Diana Kisil
 */
@ListingScope
@Subcomponent(modules = arrayOf(ListingModule::class))
interface ListingComponent {
    fun inject(target: FragmentList)
}