package com.example.testmovie.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * @author Diana Kisil
 */

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setToolbar()
        loadListingFragment(savedInstanceState)
    }

    private fun setToolbar() {
        supportActionBar?.title = getString(R.string.movie_guide)
    }

    private fun loadListingFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.listingContainer, FragmentList(), FragmentList::class.simpleName)
                .commit()
        }
    }
}