package com.example.testmovie

import android.app.Application

/**
* @author Diana Kisil
*/

class AppMovie : Application() {

    lateinit var appComponent: AppComponent
    lateinit var listingComponent: ListingComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = createAppComponent()
    }

    private fun createAppComponent(): AppComponent {
        return DaggerAppComponent.builder()
            .networkModule(NetworkModule())
            .appModule(AppModule(this)).build()
    }

    fun createListingComponent(): ListingComponent {
        listingComponent = appComponent.plus(ListingModule())
        return listingComponent
    }

    fun releaseListingComponent() {

    }

}