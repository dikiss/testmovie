package com.example.testmovie.model


import com.example.testmovie.Movie
import com.google.gson.annotations.SerializedName

/**
 * @author Diana Kisil
 */
class PopularMoviesResponse {

    @SerializedName("results")
    lateinit var movies: List<Movie>
}