package com.example.testmovie.model

import retrofit2.http.GET
import retrofit2.http.QueryMap
import rx.Observable

/**
 * @author Diana Kisil
 */
interface MovieApi {
    @GET("/3/discover/movie")
    fun getVenues(@QueryMap map: Map<String, String>): Observable<PopularMoviesResponse>
}