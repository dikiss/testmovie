package com.example.testmovie.model

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

/**
 * @author Diana Kisil
 */
@Module
class ApiModule {
    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return "http://api.themoviedb.org/"
    }

    @Provides @Singleton
    fun provideMovieDbApi(retrofit: Retrofit): MovieApi {
        return retrofit.create(MovieApi::class.java)
    }
}